SP-VLC is a simulator that is developed under [VENTOS](https://github.com/ManiAm/VENTOS_Public). SP-VLC consists of simulation that utilizes both IEEE 802.11p (DSRC) and Visible Light Communication (VLC). SP-VLC works on only Ubuntu 12 or 14 machine. To run the SP-VLC, you need to follow these steps.

**Create a folder and do following on this folder. Ensure that you gave write permission to this folder.

1-You need to install Omnet++

-update your ubuntu run update command
-run: sudo apt-get install build-essential gcc g++ bison flex perl tcl-dev tk-dev libxml2-dev zlib1g-dev default-jre doxygen graphviz libwebkitgtk-1.0-0 openmpi-bin libopenmpi-dev libpcap-dev
-install java
-OMNeT++ requires that its bin directory be in the PATH. You should add the following line to your .bashrc
export PATH=$PATH:/home/demo/Desktop/omnetpp-4.6/bin
- in the OMNeT++ directory, type the following command. Pay close attention to errors and warnings
./configure

2-You need to install Veins

-Veins is on github, and you should clone it into your computer
git clone https://github.com/sommer/veins

3-You need to import the SP-VLC

4-You need to add SP-VLC to your